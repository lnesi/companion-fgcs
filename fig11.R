options(crayon.enabled=FALSE)
options(readr.show_progress = FALSE)
library(starvz)
library(tidyverse)
library(patchwork)
library(viridis)
mkdata <- read_csv("data/exageo_dists.csv.gz") %>%
  mutate(Alg = case_when(Alg=="1D" ~ "1D-1D",
         Alg=="BC" ~ "BC on All",
         Alg=="BC FAST" ~ "BC on Fast",
         Alg=="LP-Alg" ~ "LP HetDist",
         Alg=="LPC-Alg" ~ "LP HetDist Constrained")) %>%
  mutate(Alg = factor(Alg, levels = c("BC on All", "BC on Fast",
                                      "1D-1D", "LP HetDist", "LP HetDist Constrained")))

mkdata %>%
  mutate(lp_file = paste0(Case, "_", Size)) -> raw

raw %>%
  .$lp_file %>% unique() -> lps

all_lps <- NULL
for(lp in lps){

  file <- paste0("data/lps/", lp)
  if(file.exists(file)){

    lp_r <- read_csv(file, show_col_types = FALSE) %>% slice(n()) %>% mutate(file=lp)

    if(is.null(all_lps)){
      all_lps <- lp_r
    }else{
      all_lps <- bind_rows(all_lps, lp_r)
    }
  }else{
    print(paste(file, "dont exist"))
  }

}
all_lps %>% mutate(lp_time = time/1000) %>% select(-time) %>%
  inner_join(raw, by=c("file"="lp_file")) %>%
  select(Case, Size, Machines, lp_time) %>% unique() -> lp_raw
lp_raw
                                        #diodon_md_2cle-2chi-2cho_100_2_5a

mkdata %>%
  group_by(Case, Machines, Size, Alg) -> raw

raw %>% filter(!is.na(ref)) %>%
  select(Case, Size, Alg, ref, Machines) %>% unique() -> homoge

raw %>% summarize(Mean=mean(Time), n=n(), ic=3*sd(Time)/sqrt(n())) %>%
  ggplot() +
  geom_col(aes(y=Mean, x=Alg, fill=Alg)) +
  geom_point(data=raw, aes(y=Time, x=Alg)) +
  scale_fill_brewer(palette = "Set1") +
  facet_wrap(Size ~ Machines, scales = "free", ncol=6) +
  geom_errorbar(aes(x=Alg, ymin=Mean-ic, ymax=Mean+ic), width=.5,
             ) +
  geom_segment(data=lp_raw, aes(x=5, xend=5, yend=lp_time), y=0, size=3, alpha=0.8, color="white") +
  theme_bw(base_size=20) +
  geom_text(data=homoge, aes(x=Alg, y=0, label=ref), angle=90, size=4, hjust=-0.1)  +
  scale_x_discrete(expand = c(0.05, 0)) +
  scale_y_continuous(expand = c(0.01, 0), limits=c(0, NA)) +
  guides(fill=guide_legend(title.position="top",
                           title.hjust =0.5, nrow=1,byrow=TRUE)) +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()) +
  theme(legend.margin=margin(t = 0, r = 0, b = 0, l = 0, unit='cm'),
        legend.spacing.x = unit(0.2, 'cm'),
        legend.spacing.y = unit(0.0, 'cm'),
        panel.spacing = unit(0.2, "lines"),
        legend.text = element_text(margin = margin(t = 0, b=0)),
        plot.margin=margin(c(0, 0, 0, 0)),
        legend.box.margin=margin(0,0,-15,0)
        ) +
  theme(legend.position="top") +
  labs(fill="Distribution") +
 ylab("Time [s]") -> p

library(magick)
magick_zize <- function(TARGET_FILE, plot_object, w=4, h=4)
{
    ggsave(TARGET_FILE,
           plot=plot_object,
           width=w,
           height=h,
           limit = FALSE)
    m_png <- image_trim(image_read(TARGET_FILE))
    image_write(m_png, TARGET_FILE)
    print(paste("See:", TARGET_FILE))
}
dir.create("./img/", showWarnings = FALSE, recursive = TRUE)
TARGET_FILE <- "img/perf_het.png"
magick_zize(TARGET_FILE,
            p,
            w=20, h=10)

mkdata %>%
  group_by(Case, Machines, Size, Alg) %>%
  summarize(Mean=mean(Time)) %>%
  ungroup() %>%
  select(Mean, Case, Size, Alg) -> all_c

all_c %>%
 filter(Alg == "BC on Fast") %>% select(-Alg) %>%
  rename(BC=Mean) -> bcfast
all_c %>%
 filter(Alg == "LP HetDist Constrained") %>% select(-Alg) %>%
  rename(LPC=Mean) -> lpc
bcfast %>% left_join(lpc, by=c("Case", "Size")) %>%
  mutate(gain = (BC-LPC)/BC*100) %>%
  arrange(-gain)

lp_raw
mkdata %>% filter(Case == "exa_jpdc_6che_6chi_2cho") %>%
  group_by(Alg, Size) %>%
  summarize(m=mean(Time))
